from bs4 import BeautifulSoup
import requests, json
from collections import defaultdict
from google.cloud import storage

CLOUD_STORAGE_BUCKET = 'crypto-ranking'
_storage_client = storage.Client()

IGNORES_CONVERT = ['MMCrypto', 'CrushCrypto', 'Coin Bloq', 'CryptoBred', 'PicoloResearch', 'Hacked', 'ICO Pantera']
WEIGHT_BY_BLOGGER = {
    'MMCrypto': 0, 
    'CrushCrypto': 0, 
    'Coin Bloq': 0, 
    'CryptoBred': 0, 
    'PicoloResearch': 0, 
    'Hacked': 0, 
    'ICO Pantera': 0,

    'ICO Drops': 1.0,
    'Ian Balina': 1.0,
    'OhHeyMatty': 1.0,
    'Crypto Briefing': 1.0,
    'Midgard Research': 1.0,
    'Mandy': 1.0,
    'Sergio': 1.0,
    'DiddyCarter': 1.0,
    'Lendex': 1.0,
    'TheGobOne': 1.0
}

def read_cryptodiffer():
    r = requests.get("https://cryptodiffer.com/")
    html_doc = r.text
    html_doc = html_doc.replace('<![endif]-->', '')
    soup = BeautifulSoup(html_doc, 'html.parser')

    scores = soup.find_all('div', 'ico-top-block')
    score_table = defaultdict(dict)
    href_table = defaultdict(str)
    for score in scores:
        blogger_name_td = score.find('table', 'ico-top-head').find('td', 'ico-top-blogger-name')    
        blogger_name = blogger_name_td.find('a').text

        for ts in score.find_all('table', 'ico-top-info'):
            tds = ts.find_all('td')
            ico_name = ''
            ico_href = ''
            for td in tds:
                a = td.find('a')
                if a is None:
                    score_table[blogger_name][ico_name] = td.text
                else:
                    ico_name = a.text
                    href_table[ico_name] = 'https://cryptodiffer.com' + a.attrs['href']

    return score_table, href_table

def convert_into_numberic(table):
    def convert_number_str(v_str):
        try:
            return float(v_str)
        except:
            return 0

    def convert_number_percent_str(v_str):
        try:
            return float(v_str.replace('%', ''))
        except:
            return 0

    def convert(blogger, ico_score):
        if blogger.lower() == 'ICO Drops'.lower(): # Very High, High, Medium, ..., 
            if ico_score.lower() == 'Very High'.lower():
                return 95
            else:
                return 0

        elif blogger.lower() == 'Ian Balina'.lower(): # 90, liked, ...
            return convert_number_str(ico_score)
        elif blogger.lower() == 'OhHeyMatty'.lower(): # 97, 90, ...
            return convert_number_str(ico_score)
        elif blogger.lower() == 'Crypto Briefing'.lower(): # 92%
            return convert_number_percent_str(ico_score)
        elif blogger.lower() == 'Midgard Research'.lower(): # 9.5 out of 10
            try:
                return float(ico_score.replace(' out of 10', '')) * 10
            except:
                return 0
        elif blogger.lower() == 'Mandy'.lower(): # 9, 8.7, ...
            return convert_number_str(ico_score) * 10
        elif blogger.lower() == 'Sergio'.lower(): # 96%, ...
            return convert_number_percent_str(ico_score)
        elif blogger.lower() == 'DiddyCarter'.lower(): # A+, A-, ...
            if ico_score == 'A+':
                return 95
            elif ico_score == 'A':
                return 90
            elif ico_score == 'A-':
                return 85
            elif ico_score == 'B+':
                return 80
            elif ico_score == 'B':
                return 75
            elif ico_score == 'B-':
                return 70

        elif blogger.lower() == 'Lendex'.lower(): # 89, ...
            return convert_number_str(ico_score)
        elif blogger.lower() == 'TheGobOne'.lower(): # 93%, ...
            return convert_number_percent_str(ico_score)
        else:
            return 0

    converted = defaultdict(dict)
    for blogger, reviews in table.items():
        if blogger in IGNORES_CONVERT:
            continue
        for ico_name, ico_score in reviews.items():
            score = convert(blogger, ico_score)
            if score == 0: continue
            converted[blogger][ico_name] = score
    return converted

def get_score_and_count_per_coin(numeric_table):
    counts = defaultdict(float)
    sums = defaultdict(float)
    for blogger, reviews in numeric_table.items():
        for ico_name, ico_score in reviews.items():       
            weight = WEIGHT_BY_BLOGGER[blogger] if blogger in WEIGHT_BY_BLOGGER else 0
            counts[ico_name] += weight
            sums[ico_name] += weight * ico_score

    scores = defaultdict(dict)
    for sn, sv in sums.items():
        scores[sn] = (sv / counts[sn], counts[sn])
    return scores

def sort_by_score(coin_and_score):
    l = [(c, s[0], s[1]) for c, s in coin_and_score.items() if s[1] > 2]
    ls = sorted(l, key=lambda pair: -pair[1])
    return ls

def get_ranking_and_score_and_counts():
    score_table, href_table = read_cryptodiffer()
    converted = convert_into_numberic(score_table)
    scores = get_score_and_count_per_coin(converted)
    ls = sort_by_score(scores)
    ls_with_href = [s + (href_table[s[0]],) for s in ls]
    return ls_with_href

def get_ranking_and_score_and_counts_from_gcs():
    return json.loads(requests.get('https://storage.googleapis.com/crypto-ranking/ranking.json').text)

def fetch_and_update_to_gcs():
    ranking = get_ranking_and_score_and_counts()
    blob = _storage_client.get_bucket(CLOUD_STORAGE_BUCKET).blob('ranking.json')
    blob.make_public()
    blob.upload_from_string(json.dumps(ranking))

if __name__ == '__main__':
    import pprint
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(json.loads(requests.get('https://storage.googleapis.com/crypto-ranking/ranking.json').text))
    pp.pprint(requests.get('http://35.201.127.76').text)
    ranking = get_ranking_and_score_and_counts_from_gcs()
    pp.pprint(ranking)

