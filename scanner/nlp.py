from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types
from oauth2client.service_account import ServiceAccountCredentials

SERVICE_ACCOUNT_CRED_FILE = '/path/to/keyfile.json'
client = None

def init_client():
    global client
    if client is not None: return

    #scopes = ['https://www.googleapis.com/auth/sqlservice.admin']
    #credentials = ServiceAccountCredentials.from_json_keyfile_name(SERVICE_ACCOUNT_CRED_FILE , scopes)
    client = language.LanguageServiceClient() # credentials = credentials

def extract_entities(text):
    init_client()
    document = types.Document(
        content=text,
        type=enums.Document.Type.PLAIN_TEXT)

    response = client.analyze_entities(document=document)
    return [e.name.title() for e in response.entities]

def analyze_sentiment(text):
    init_client()
    if text is None: return None
    document = types.Document(
        content=text,
        type=enums.Document.Type.PLAIN_TEXT)

    response = client.analyze_sentiment(
        document = document,
        encoding_type = 'UTF32')

    return response.document_sentiment