# Create your views here.
from django.http import JsonResponse
from . import entity

top_entities_list = []

def index(request):
    global top_entities_list

    if len(top_entities_list) == 0 or top_entities_list[-1]['datetime'] != entity.get_file_name():
        tes = entity.get()
        top_entities_list.append({'datetime': entity.get_file_name(), "entities": tes})
    tes = top_entities_list[-1]["entities"]

    tes = entity.get()
    return JsonResponse(tes, safe=False)
