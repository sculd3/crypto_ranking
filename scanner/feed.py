#!/usr/bin/env python

# [START imports]
import logging, requests, json, datetime, os
import feedparser
import xml.etree.ElementTree as etree
from bs4 import BeautifulSoup
from google.cloud import storage

NEWSAPI_API_KEY = '0d381adbccf6423dbdaa98823ec6f33c'
NEWSAPI_SOURCES_URL = 'https://newsapi.org/v1/sources?language=en'
NEWSAPI_ARTICLE_URL = 'https://newsapi.org/v1/articles?source=%s&sortBy=top&apiKey=%s'
CLOUD_STORAGE_BUCKET = 'sonar-ml'

SOURCES_RESPONSE_SOURCES = 'sources'

_storage_client = storage.Client()

def json_http(url):
    r = requests.get(url)
    if r.status_code != 200:
        return None
    return r.json()

def get_sources():
    result = json_http(NEWSAPI_SOURCES_URL)
    if result is None:
        return
    return list(result['sources'])

def fetch_rss_feed(url, source_name, category):
    feed = feedparser.parse(url)
    return [{
        'title':e['title'],
        'description':e['description'],
        'url':e['link'],
        'source': source_name,
        'category': category
        } for e in feed['entries']]

def fetch_newsbitc_feed():
    return fetch_rss_feed('http://www.newsbtc.com/feed/', 'newsbitc', 'Cryptocurrency')

def fetch_cointelegraph_feed():
    return fetch_rss_feed('https://cointelegraph.com/rss', 'cointelegraph', 'Cryptocurrency')

def fetch_bitcoinnews_feed():
    return fetch_rss_feed('https://news.bitcoin.com/feed', 'news.bitcoin', 'Cryptocurrency')

def bitcoinmagazine_feed():
    return fetch_rss_feed('https://bitcoinmagazine.com/feed', 'bitcoinmagazine', 'Cryptocurrency')

def tokenmarketblog_feed():
    return fetch_rss_feed('https://tokenmarket.net/blog/rss', 'tokenmarketblog', 'Cryptocurrency')

def fetch_livebitcoinnews_feed():
    return fetch_rss_feed('http://www.livebitcoinnews.com/feed/', 'livebitcoinnews', 'Cryptocurrency')

def fetch_bitcoin_feeds():
    feeds = fetch_newsbitc_feed() + \
            fetch_cointelegraph_feed() + \
            fetch_bitcoinnews_feed() + \
            fetch_livebitcoinnews_feed() + \
            bitcoinmagazine_feed() + \
            tokenmarketblog_feed()
    return feeds

def fetch_wsj_feed():
    return fetch_rss_feed('http://www.wsj.com/xml/rss/3_7085.xml', 'wsj', 'World') +\
        fetch_rss_feed('http://www.wsj.com/xml/rss/3_7014.xml', 'wsj', 'US') + \
        fetch_rss_feed('http://www.wsj.com/xml/rss/3_7031.xml', 'wsj', 'Markets') + \
        fetch_rss_feed('http://www.wsj.com/xml/rss/3_7455.xml', 'wsj', 'Tech')

def fetch_cnbc_feed():
    return fetch_rss_feed('http://www.cnbc.com/id/100727362/device/rss/rss.html', 'cnbc', 'World') + \
           fetch_rss_feed('http://www.cnbc.com/id/15837362/device/rss/rss.html', 'cnbc', 'US') + \
           fetch_rss_feed('http://www.cnbc.com/id/19832390/device/rss/rss.html', 'cnbc', 'Asia') + \
           fetch_rss_feed('http://www.cnbc.com/id/19794221/device/rss/rss.html', 'cnbc', 'Europe') + \
           fetch_rss_feed('http://www.cnbc.com/id/10001147/device/rss/rss.html', 'cnbc', 'Business') + \
           fetch_rss_feed('http://www.cnbc.com/id/15839135/device/rss/rss.html', 'cnbc', 'Earnings') + \
           fetch_rss_feed('http://www.cnbc.com/id/100370673/device/rss/rss.html', 'cnbc', 'Commentary') + \
           fetch_rss_feed('http://www.cnbc.com/id/20910258/device/rss/rss.html', 'cnbc', 'Economy') + \
           fetch_rss_feed('http://www.cnbc.com/id/10000664/device/rss/rss.html', 'cnbc', 'Finance') + \
           fetch_rss_feed('http://www.cnbc.com/id/19854910/device/rss/rss.html', 'cnbc', 'Tech') + \
           fetch_rss_feed('http://www.cnbc.com/id/10000113/device/rss/rss.html', 'cnbc', 'Politics') + \
           fetch_rss_feed('http://www.cnbc.com/id/10000108/device/rss/rss.html', 'cnbc', 'Health Care') + \
           fetch_rss_feed('http://www.cnbc.com/id/10000115/device/rss/rss.html', 'cnbc', 'Real Estate') + \
           fetch_rss_feed('http://www.cnbc.com/id/10001054/device/rss/rss.html', 'cnbc', 'Wealth') + \
           fetch_rss_feed('http://www.cnbc.com/id/10000101/device/rss/rss.html', 'cnbc', 'Autos') + \
           fetch_rss_feed('http://www.cnbc.com/id/19836768/device/rss/rss.html', 'cnbc', 'Energy') + \
           fetch_rss_feed('http://www.cnbc.com/id/10000110/device/rss/rss.html', 'cnbc', 'Media') + \
           fetch_rss_feed('http://www.cnbc.com/id/10000116/device/rss/rss.html', 'cnbc', 'Retail') + \
           fetch_rss_feed('http://www.cnbc.com/id/10000739/device/rss/rss.html', 'cnbc', 'Travel') + \
           fetch_rss_feed('http://www.cnbc.com/id/44877279/device/rss/rss.html', 'cnbc', 'Small Business')

def _get_time_str():
    #return datetime.datetime.now().strftime("%Y-%m-%dT%H")
    return '2018-03-29T21'

def get_blob_name():
    #return 'news_fetch/%s' % (datetime.datetime.now().strftime("%Y-%m-%dT%H"))
    return 'news_fetch/%s' % (_get_time_str())

def fetch_newsapi_all_sources():
    blob = _storage_client.get_bucket(CLOUD_STORAGE_BUCKET).blob(get_blob_name())
    if blob.exists():
        articles_str = blob.download_as_string()
        return json.loads(articles_str)

    def fetch_newsapi(src):
        url = NEWSAPI_ARTICLE_URL % (src['id'], NEWSAPI_API_KEY)
        js = json_http(url)
        if js is None: return None
        ars = js['articles']
        for ar in ars:
            ar['category'] = str(src['category'])
            ar['source'] = str(src['id'])
        return ars

    sources = get_sources()
    articles = {}
    for i, src in enumerate(sources):
        print("[fetch_newsapi_all_sources] extracting articles from %s, %d out of %d" % (src['id'], i, len(sources)))
        ars = fetch_newsapi(src)
        if ars is None:
            print('[fetch_newsapi_all_sources] no articles fetched from %s' % (src['name']))
            continue
        articles.update({ar['url']: ar for ar in ars})

    if len(articles) > 0:
        blob.upload_from_string(json.dumps(articles))

    return articles

def fetch():
    bitcoin_feeds = fetch_bitcoin_feeds()
    other_feeds = fetch_wsj_feed() + fetch_cnbc_feed()
    res = other_feeds + bitcoin_feeds

    blob = _storage_client.get_bucket(CLOUD_STORAGE_BUCKET).\
        blob('news_fetch/%s' % (datetime.datetime.now().strftime("%Y-%m-%dT%H")))
    if not blob.exists():
        blob.upload_from_string(str(res))

    return res

if __name__ == '__main__':
    fetch()
    for i in fetch_cnbc_feed():
        print(i)

