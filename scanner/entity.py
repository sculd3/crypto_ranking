#!/usr/bin/env python

# [START imports]
import logging, requests, json, datetime, os
from collections import defaultdict, Counter
import scanner.nlp as nlp
#import scanner.nlp_rosette as nlp_resette
#import scanner.nlp_paralleldots as nlp_paralleldots
#import scanner.nlp_dandelion as nlp_dandelion
import threading, feedparser
from threading import Thread
import xml.etree.ElementTree as etree
from bs4 import BeautifulSoup
import scanner.feed as feed
from google.cloud import storage
import scanner.feed as scanner_feed

DAILY_ENTITIES_DIR = 'daily_entities'
CLOUD_STORAGE_BUCKET = 'sonar-ml'

_storage_client = storage.Client()

def _get_time_str():
    #return datetime.datetime.now().strftime("%Y-%m-%dT%H")
    return '2018-03-29T21'

def get_extracted_entities_blob_name():
    return 'extracted_entities/%s' % (_get_time_str())

def get_combined_entities_blob_name():
    return 'combined_entities/%s' % (_get_time_str())

def get_sentiment_decorated():
    return 'sentiment_decorated/%s' % (_get_time_str())

def extract_entities():
    blob = _storage_client.get_bucket(CLOUD_STORAGE_BUCKET).blob(get_extracted_entities_blob_name())
    if blob.exists():
        extract_entities = blob.download_as_string()
        return json.loads(extract_entities)

    articles = scanner_feed.fetch_newsapi_all_sources()
    extract_entities = defaultdict(list)
    i = 0
    try:
        for url, ar in articles.items():
            #if i > 550: break
            print("[extract_entities] extracting entities of %s..., %d out of %d" % (ar['title'][:10], i, len(articles)))
            entities = nlp.extract_entities(ar['title'])
            for e in entities:
                extract_entities[e].append(
                    {'name': e, 'article': url, 'entities': entities}
                )
            i += 1

    except:
        print('excpetion')

    blob.upload_from_string(json.dumps(extract_entities))
    return extract_entities

def combine_entities():
    blob = _storage_client.get_bucket(CLOUD_STORAGE_BUCKET).blob(get_combined_entities_blob_name())
    if blob.exists():
        combined = blob.download_as_string()
        return json.loads(combined)

    articles = scanner_feed.fetch_newsapi_all_sources()
    entities = extract_entities()

    combined = {}
    for e, related_es in entities.items():
        cat_and_sources = defaultdict(set)
        for re in related_es:
            url = re['article']
            a = articles[url]
            c = a['category']
            cat_and_sources[c].add(url)
        cat_and_sources = {k: list(v) for k, v in cat_and_sources.items()}
        es = []
        for re in related_es:
            es += re['entities']
        combined[e] = {'category': cat_and_sources, 'entities': dict(Counter(es))}

    sorted_keys = sorted(combined.keys(), key = lambda e: -len(combined[e]['entities']))

    combine_sorted = []
    for k in sorted_keys:
        related_es = []
        for p in Counter(combined[k]['entities']).most_common():
            e, cnt = p[0], p[1]
            if p[0] == k: continue
            # related, cnt, its categories
            related_es.append((e, cnt, list(combined[e]['category'].keys())))
        combine_sorted.append({k: {'category': combined[k]['category'], 'entities': related_es}})

    blob.upload_from_string(json.dumps(combine_sorted))
    return combined

def decorate_sentiment_to_combined():
    blob = _storage_client.get_bucket(CLOUD_STORAGE_BUCKET).blob(get_sentiment_decorated())
    if blob.exists():
        combined = blob.download_as_string()
        return json.loads(combined)

    articles = scanner_feed.fetch_newsapi_all_sources()
    entities = extract_entities()
    combines = combine_entities()

    decorated = []
    try:
        for combine in combines:
            for e, detail in combine.items():
                titles = []
                for cat in detail['category']:
                    for url in detail['category'][cat]:
                        titles.append(articles[url]['title'])

                text = ' '.join(titles)
                print('analyzing sentiment of %s' % (text[:20]))
                sent = nlp.analyze_sentiment(text)
                if sent is None: continue
                detail['sentiment'] = {'magnitude': sent.magnitude, 'score': sent.score}
            decorated.append(combine)

    except:
        print('exception occured during sentimental analysis')

    blob.upload_from_string(json.dumps(decorated))
    return decorated

if __name__ == '__main__':
    feed = feed.fetch_wsj_feed()[:1]