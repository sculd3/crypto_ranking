import requests

DANDELION_TOKEN = '0727850167a74702a9e5ac29dcaf0c52'
ENDPOINT_ENTITY_EXTRACTION = 'https://api.dandelion.eu/datatxt/nex/v1?token=%s&text=%s'
ENDPOINT_SENTIMENT_ANALYSIS = 'https://api.dandelion.eu/datatxt/sent/v1?token=%s&text=%s'

def extract_entities(text):
    url = ENDPOINT_ENTITY_EXTRACTION % (DANDELION_TOKEN, text)
    r = requests.get(url)
    if r.status_code != 200:
        return []
    js = r.json()
    return [a['title'] for a in js['annotations']]

def analyze_sentiment(text):
    url = ENDPOINT_SENTIMENT_ANALYSIS % (DANDELION_TOKEN, text)
    r = requests.get(url)
    if r.status_code != 200:
        return []
    js = r.json()
    return js['sentiment']

if __name__ == '__main__':
    text = 'hello world sunrise capital ico'
    js = extract_entities(text)
    print(js)
