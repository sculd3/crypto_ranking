from rosette.api import API

API_KEY = 'abe0d15ea2bb6dd521a125157af3bfc8'

from rosette.api import API, DocumentParameters, RosetteException

api = API(user_key=API_KEY, service_url='https://api.rosette.com/rest/v1/')

def extract_entities(text):
    params = DocumentParameters()
    params["content"] = text
    try:
        es = api.entities(params)
        return [e['normalized'] for e in es['entities']]
    except RosetteException as exception:
        print(exception)
    return []

def analyze_sentiment(text):
    return {}

if __name__ == '__main__':
    text = "North Korea could test hydrogen bomb over Pacific Ocean, says foreign minister"
    e = analyze_sentiment(text)
    print(e)

