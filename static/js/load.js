var app = angular.module('icoRanking', []).config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

app.controller('rankingControl', function($scope, $http) {
    $http.get("/ranking")
        .then(function (response) {
            $scope.rankings = response.data;
        });
});
